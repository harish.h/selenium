package TestCases;

import org.openqa.selenium.WebDriver;

import Comman_function.Handle_browser;

public class Locators { 
	
	public static void main(String[] args) {
		
		WebDriver chrome_driver=Handle_browser.InstantiateBrowser("Chrome");
		chrome_driver.get("https://www.Amazon.com/");
		chrome_driver.quit();
		WebDriver firefox_driver=Handle_browser.InstantiateBrowser("Firefox");
		firefox_driver.get("https://www.Amazon.com/");
		firefox_driver.quit();
		WebDriver edge_driver=Handle_browser.InstantiateBrowser("Edge");
		edge_driver.get("https://www.Amazon.com/");
		edge_driver.quit();
	}

} 
