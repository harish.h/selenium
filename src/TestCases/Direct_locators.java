package TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import Comman_function.Handle_browser;

public class Direct_locators {
	
	public static void main (String[] args) {
		
		WebDriver driver =Handle_browser.InstantiateBrowser("Chrome");
		driver.get("https://www.amazon.in/");
		driver.manage().window().maximize();
//		driver.findElement(By.id("nav-logo-sprites")).click();
//		Thread.sleep(5000);
//		driver.findElement(By.className("nav-progressive-content")).click();
//		driver.findElement(By.linkText("Best Sellers")).click();
		driver.findElement(By.partialLinkText("miniTV")).click();
		driver.quit();

	}

}
