package Seleniumpackage;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Comman_function.Handle_browser;

public class Calendar {

			public static void main(String[] args) throws InterruptedException {
				 WebDriver Driver = Handle_browser.InstantiateBrowser("Chrome");
				 Handle_browser.LaunchURL(Driver, "https://www.easemytrip.com/");
				 
				 //Implicit wait
				 Driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3000));
				 
				 //Calendar webelements
				 WebElement DepartDate = Driver.findElement(By.xpath("//input[@placeholder=\"Departure\"]"));
				 WebElement ReturnDate = Driver.findElement(By.xpath("//input[@placeholder=\"Return\"]/.."));
				 
				 //Current Departure Date
				 DepartDate.click();
				 
				 WebElement CurrentDate = Driver.findElement(By.xpath("//li[@class=\"active-date\"]"));
				 CurrentDate.click();
				
				 
				 //Any and Return Date
				 ReturnDate.click();
				 
				 List<WebElement> Date_Click= Driver.findElements
						 (By.xpath("//div[text()=\"Feb 2024\"]/../..//li[@class=\"\" and @onclick=\"SelectDate(this.id)\"]"));
				 
				 int count = Date_Click.size();
				 
				 for(int i=0;i<count;i++) {
					 
					 String date_value = Date_Click.get(i).getText();
					 System.out.println(date_value);
					 
					 if(date_value.contains("14")) {
						 Date_Click.get(i).click();
						 break;
					 }
					 else {
						 System.out.println("Desired date is not found in current iteration : " +i+ " , Hence retrying");
					 }
				 }
				
				 Driver.quit();

			}

		}


