package Seleniumpackage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Comman_function.Handle_browser;

public class Amazon {

	WebDriver driver;

	public Amazon(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@id=\"nav-xshop\"]/a[text()=\"Customer Service\"]")
	WebElement CustomerService;

	public void CustomerService() {
		Handle_browser.Validate_Click(driver, CustomerService, 5);

	}
}
